package com.gpmall.pay.biz.payment.constants;

import com.gupaoedu.pay.IEnum;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * 腾讯课堂搜索 咕泡学院
 * 加群获取视频：608583947
 *
 * @author 风骚的Michael 老师
 */
@RequiredArgsConstructor
@Getter
public enum PayResultEnum implements IEnum {

    TRADE_PROCESSING("1", "支付处理中"),
    TRADE_FINISHED("2", "支付完成"),
    TRADE_SUCCESS("3", "支付成功"),
    FAIL("4", "支付失败");

    private final String code;

    private final String desc;


}
