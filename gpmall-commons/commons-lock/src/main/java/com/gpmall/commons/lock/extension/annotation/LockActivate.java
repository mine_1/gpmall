package com.gpmall.commons.lock.extension.annotation;

import java.lang.annotation.*;

/**
 * @author z.w
 * @version v1.0
 * @apiNote 激活标志
 * @date 2021/12/16 10:28
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface LockActivate {
    /**
     * key
     * @return
     */
    String[] value() default {};

    /**
     * 顺序
     * @return
     */
    int order() default 0;
}
