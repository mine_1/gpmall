package com.gpmall.commons.lock.filter;

import com.gpmall.commons.lock.extension.annotation.LockSpi;

/**
 * @author z.w
 * @version v1.0
 * @apiNote desc
 * @date 2021/12/16 11:47
 */
@LockSpi
public interface LockFilter {

    void init();
}
