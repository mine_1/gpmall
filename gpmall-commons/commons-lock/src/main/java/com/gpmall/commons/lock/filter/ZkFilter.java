package com.gpmall.commons.lock.filter;

import com.gpmall.commons.lock.extension.annotation.LockActivate;

/**
 * @author z.w
 * @version v1.0
 * @apiNote desc
 * @date 2021/12/16 11:48
 */
@LockActivate(value = "zkFilter",order = 1)
public class ZkFilter implements LockFilter{
    @Override
    public void init() {
        System.out.println("zk");
    }
}
