package com.gpmall.commons.lock.impl;

import com.gpmall.commons.lock.DistributedLock;
import com.gpmall.commons.lock.extension.ExtensionLoader;
import com.gpmall.commons.lock.filter.LockFilter;

import java.util.List;

/**
 * @author z.w
 * @version v1.0
 * @apiNote desc
 * @date 2021/12/16 11:36
 */
public class SpiTest {
    public static void main(String[] args) {
//        DistributedLock zookeeper = ExtensionLoader.getExtensionLoader(DistributedLock.class).getExtension("zookeeper");
//        System.out.println(zookeeper);

        List<LockFilter> activateExtension = ExtensionLoader.getExtensionLoader(LockFilter.class).getActivateExtension();
        System.out.println(activateExtension);
    }
}
